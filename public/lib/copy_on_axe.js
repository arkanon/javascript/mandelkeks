// Arkanon <paulo.bagatini@softplan.com.br>
// 2018/10/21 (Sun) 17:43:11 -03

   iteracts = 300;
   cornerBL = [ -1.944 , -0.00124 ];
   cornerTR = [ -1.939 ,  0.00124 ];

   // <http://colorzilla.com/gradient-editor>
   gradient = [
                [ [ 255, 255, 255 ],   0 ],
                [ [   1,  15, 170 ],  10 ],
                [ [  26, 193,  23 ],  50 ],
                [ [ 240, 244,   2 ],  60 ],
                [ [ 117,  34,   1 ],  80 ],
                [ [   0,   0,   0 ], 300 ],
              ];

// EOF
