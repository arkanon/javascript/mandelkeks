// Arkanon <paulo.bagatini@softplan.com.br>
// 2018/10/21 (Sun) 17:34:55 -03

   iteracts = 300;
   canvasW  = 500;
   cornerBL = [ -2.01 , -2.01 ];
   cornerTR = [  2.01 ,  2.01 ];

   // <http://colorzilla.com/gradient-editor>
   gradient = [
                [ [ 255, 255, 255 ],   0 ],
                [ [   1,  15, 170 ],  10 ],
                [ [  26, 193,  23 ],  20 ],
                [ [ 240, 244,   2 ],  30 ],
                [ [ 117,  34,   1 ],  40 ],
                [ [   0,   0,   0 ], 300 ],
              ];

// EOF
