


    function position(e)
    // <http://stackoverflow.com/questions/55677>
    {

      var X, Y, x, y;

      if (e.pageX && e.pageY)
      {
        X = e.pageX;
        Y = e.pageY;
      }
      else
      {
        X = e.clientX + document.body.scrollLeft + document.documentElement.scrollLeft;
        Y = e.clientY + document.body.scrollTop  + document.documentElement.scrollTop;
      }
      X -= plano.offsetLeft;
      Y -= plano.offsetTop;

      x = (   X - parseInt(plano.width /2) )*passo + centro['x'];
      y = ( - Y + parseInt(plano.height/2) )*passo + centro['y'];

      return { 'X':X , 'Y':Y , 'x':x , 'y':y };

    }



    function define_canvas()
    {

      plano.width  = window.innerWidth;
      plano.height = window.innerHeight;

      eixos.width  = window.innerWidth;
      eixos.height = window.innerHeight;

      ctx_plano.fillStyle = 'lightblue';
      ctx_plano.fillRect( 0, 0, plano.width, plano.height );

      ctx_plano.strokeStyle = 'blue';
      ctx_plano.lineWidth   = 1;

      ctx_plano.beginPath();
      ctx_plano.moveTo( .5              , parseInt(plano.height/2)+.5 );
      ctx_plano.lineTo( plano.width-.5 , parseInt(plano.height/2)+.5 );
      ctx_plano.stroke();

      ctx_plano.beginPath();
      ctx_plano.moveTo( parseInt(plano.width/2)+.5 , .5             );
      ctx_plano.lineTo( parseInt(plano.width/2)+.5 , plano.height-.5 );
      ctx_plano.stroke();

      data = ctx_plano.getImageData(0, 0, plano.width, plano.height);

    }



    function draw_eixos()
    {

      eixos.width = eixos.width;
      ctx_eixos.lineWidth   = 1;
      ctx_eixos.lineCap     = 'butt';

      ctx_eixos.strokeStyle = 'red';
      ctx_eixos.beginPath();
      ctx_eixos.dashedLineFromTo( [ .5 , parseInt(pos['Y'])+.5 ] , [ plano.width-.5 , parseInt(pos['Y'])+.5 ] );
      ctx_eixos.dashedLineFromTo( [ parseInt(pos['X'])+.5 , .5  ] , [ parseInt(pos['X'])+.5 , plano.height-.5 ] );
      ctx_eixos.closePath();
      ctx_eixos.stroke();

      ctx_eixos.strokeStyle = 'white';
      ctx_eixos.beginPath();
      ctx_eixos.dashedLineFromTo( [ .5 , parseInt(centro['Y'])+.5 ] , [ plano.width-.5 , parseInt(centro['Y'])+.5 ] );
      ctx_eixos.dashedLineFromTo( [ parseInt(centro['X'])+.5 , .5  ] , [ parseInt(centro['X'])+.5 , plano.height-.5 ] );
      ctx_eixos.closePath();
      ctx_eixos.stroke();

    }



    CanvasRenderingContext2D.prototype.dashedLineFromTo = function(from,to,dashLength)
    // <http://webdood.com/?p=225>
    {

      var __dashedLineFromTo =
      {
        isDrawing : true,
        unFinishedPixelsFromLastDash : 0
      }

      var x                 = from[0],
          y                 = from[1],
          dx                = (to[0] - x) + .00000001,
          dy                =  to[1] - y,
          slope             = dy/dx,
          distanceRemaining = Math.sqrt(dx*dx + dy*dy),
          bUnfinishedPixels = false,
          theDashLength,
          xStep;

      if (dashLength == undefined) dashLength = 3,

      this.moveTo(x,y);

      while (distanceRemaining>=0.1)
      {

        if (__dashedLineFromTo.unFinishedPixelsFromLastDash === 0)
        {
          theDashLength = dashLength;
        }
        else
        {
          theDashLength = __dashedLineFromTo.unFinishedPixelsFromLastDash;
          __dashedLineFromTo.unFinishedPixelsFromLastDash = 0;
          __dashedLineFromTo.isDrawing = !__dashedLineFromTo.isDrawing
        }

        if (dashLength > distanceRemaining)
        {
          dashLength = distanceRemaining;
          bUnfinishedPixels=true;
        }

        xStep = Math.sqrt( theDashLength*theDashLength / (1 + slope*slope) );
        x    += xStep;
        y    += slope*xStep;
        this[__dashedLineFromTo.isDrawing ? 'lineTo' : 'moveTo'](x,y);
        distanceRemaining -= theDashLength;
        __dashedLineFromTo.isDrawing = !__dashedLineFromTo.isDrawing;

      }

      if (bUnfinishedPixels)
      {
        __dashedLineFromTo.unFinishedPixelsFromLastDash = theDashLength;
      }

    }



