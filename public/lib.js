


    function computed(prop)
    {
      return parseFloat(window.getComputedStyle(body).getPropertyValue(prop));
    }



    function time(this_current_time, this_start_time, this_end_time, this_time_difference)
    // Elapsed Time Calculator
    // by David Tam <http://www.ComputationalNeuralSystems.com/>
    //
    // Usage:
    //    var delay = new aux.timestamp(0,0,0,0);
    //    delay.StartTiming();
    // // { javascript code }
    //    esc(delay.EndTiming());
    {
      this.this_current_time    = this_current_time;
      this.this_start_time      = this_start_time;
      this.this_end_time        = this_end_time;
      this.this_time_difference = this_time_difference;

      this.GetCurrentTime = function()
      // get current time from date timestamp
      {
        var my_current_timestamp;
            my_current_timestamp = new Date();						// stamp current date & time
        return my_current_timestamp.getTime();
      }

      this.StartTiming = function()
      // stamp current time as start time and reset display textbox
      {
        this.this_start_time = this.GetCurrentTime();					// stamp current time
      }

      this.EndTiming = function()
      // stamp current time as stop time, compute elapsed time difference and display in textbox
      {
        this.this_end_time = this.GetCurrentTime();					// stamp   current time
        this.this_time_difference = (this.this_end_time - this.this_start_time) / 1000;	// compute elapsed time
        return this.this_time_difference;						// return  elapsed time
      }

    }



    function getParameterByName(name, splitstr, type)
    // <http://stackoverflow.com/a/901144>
    {
      name = name.replace(/[\[]/, "\\\[").replace(/[\]]/, "\\\]");
      var regexS  = "[#;]" + name + "=([^\\?&#;]*)";
      var regex   = new RegExp(regexS);
      var results = regex.exec(location.hash);
      if ( results === null )
      {
        return undefined;
      }
      else
      {
        var value = decodeURIComponent(results[1].replace(/\+/g, " "));
        if ( splitstr === undefined )
        {
          if ( type === undefined || type === 'f' ) value = eval(value);
        }
        else
        {
          value = value.split(splitstr)
          // <http://stackoverflow.com/a/4291498>
          if ( type === undefined || type === 'f' ) value = value.map(function(x){return parseFloat(x)});
        }
        return value;
      }
    }



    // setZeroTimeout <http://dbaron.org/log/20100309-faster-timeouts>
    (
      // add only to the window object, and hide everything else in a closure.
      function()
      {
        var timeouts = [];
        var messageName = "zero-timeout-message";
        // Like setTimeout, but only takes a function argument. There's no time argument
        // (always zero) and no arguments (you have to use a closure).
        function setZeroTimeout(fn)
        {
          timeouts.push(fn);
          window.postMessage(messageName, "*");
        }
        function handleMessage(event)
        {
          if (event.source == window && event.data == messageName)
          {
            event.stopPropagation();
            if (timeouts.length > 0)
            {
              var fn = timeouts.shift();
              fn();
            }
          }
        }
        window.addEventListener("message", handleMessage, true);
        // Add the one thing we want added to the window object.
        window.setZeroTimeout = setZeroTimeout;
      }
    )();



