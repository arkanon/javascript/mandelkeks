


    function mainloop()
    {
      var cor;
      col = 0;
      for (Cr=L[0]; Cr<=R[0]; Cr+=S)
      {
        ins = inSet(Cr, Ci, I, sqmodlim);
          M = Math.sqrt(ins[1]);
          m = Math.sqrt(ins[2]);
          N =           ins[3];

        // <http://stackoverflow.com/a/25816111>
        // Based on <http://jsfiddle.net/vksn3yLL> from <http://stackoverflow.com/a/30144587>
        //  <http://jsfiddle.net/arkanon/vksn3yLL/1328>     interactive
        //  <http://jsfiddle.net/arkanon/vksn3yLL/1316> non-interactive
        var slider        = N;
        var colorRange = [ gradient.length-2, gradient.length-1 ];
        for ( var index in gradient ) if ( slider <= gradient[index][1] ) { colorRange = [ index-1, index ]; break }

        // get the two closest colors
        var firstcolor    = gradient[colorRange[0]][0];
        var secondcolor   = gradient[colorRange[1]][0];

        // calculate ratio between the two closest colors
        var firstcolor_x  = gradient[colorRange[0]][1]/100;
        var secondcolor_x = gradient[colorRange[1]][1]/100 - firstcolor_x;
        var slider_x      = slider/100                     - firstcolor_x;
        var ratio         = slider_x/secondcolor_x;

            cor           = ( ins[0] || N==1 ) ? mandelcor : pickHex( secondcolor, firstcolor, ratio );

        plotpix(row, col, cor);
        col++;
      }
      Ci -= S;
      row++;
      context.putImageData(dataset, 0, 0);
      if ( Ci>=L[1] ) setZeroTimeout(mainloop); else console.log("Renderizado em "+delay.EndTiming()+" segundos");
    }



    function  inSet(Cr, Ci, I, sqmodlim)
    {
      var tmp, znr, zni, znm2, n;
      znr = Z0r;
      zni = Z0i;
      for (n=1; n<=I; n++)
      {
        // <http://maxima.cesga.es>
        // z²+c
        tmp  = 2*znr*zni + Ci;
        znr  = znr*znr - zni*zni + Cr;
        zni  = tmp;
        znm2 = znr*znr + zni*zni;
        cm2  = Cr*Cr + Ci*Ci;
        if (znm2 >= sqmodlim) return [false, znm2, cm2, n];
      }
      return [true, znm2, cm2];
    }



    function plotpix(row, col, cor)
    {
      var i = ( row * W + col )*4;
      dataset.data[i+0] = cor[0];
      dataset.data[i+1] = cor[1];
      dataset.data[i+2] = cor[2];
      dataset.data[i+3] = 255;
    }



    function pickHex(color1, color2, weight)
    {
      var p   = weight;
      var w   = 2*p - 1;
      var w1  = (w+1)/2;
      var w2  = 1 - w1;
      var rgb = [
                  Math.round( w1*color1[0] + w2*color2[0] ),
                  Math.round( w1*color1[1] + w2*color2[1] ),
                  Math.round( w1*color1[2] + w2*color2[2] ),
                ];
      return rgb;
    }



