Mandelkeks
==========

Mandelbrot/Julia Set generator, explorer and zoomer in JS+Canvas

- http://ventrella.com/mandelbrot_orbits
- http://youtube.com/watch?v=9gk_8mQuerg
- http://youtube.com/watch?v=FFftmWSzgmk
- http://juliasets.dk/Pictures_of_Julia_and_Mandelbrot_sets.htm
- http://math.univ-toulouse.fr/~cheritat/wiki-draw/index.php/Mandelbrot_set

